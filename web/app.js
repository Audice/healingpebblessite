const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
app.use(express.static('public'));
app.get('/pulse', (req, res) => {
    fs.readFile('../pulse.txt', 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return;
        }
        res.send(data);
    });
});


app.get('/signal', (req, res) => {
    let file_path = "../signal";
    let stats = fs.statSync(file_path)
    let fileSizeInBytes = stats.size;
    const stream = fs.createReadStream(file_path, {start: fileSizeInBytes - 10000, end: fileSizeInBytes});
    let chunks = [];
    stream.on('data', chunk => chunks.push(chunk)).on('close', () => {
        let all_data = Buffer.concat(chunks);
        let tmp_buffer = new Uint8Array(all_data).buffer;
        let buffer = new DataView(tmp_buffer);
        let data = [];
        for (let i = 0; i < all_data.length; i += 2) {
            data.push((buffer.getInt16(i, true) - 2048) / 2000);
        }
        res.send(data);
    })
});

app.listen(process.env.port || 3000);
console.log('Running at Port 3000');
