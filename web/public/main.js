function UpdatePulse() {
    let div = document.getElementById("pulse");
    $.ajax({
        url: "/pulse",
        type: 'GET',
        dataType: 'json',
        success: (res) => {
            div.innerText = res;
        }
    });
}


function DrawData(data) {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (data.length < 5000) {
        return;
    }
    console.log(canvas.clientWidth);
    canvas.style.height = (4 / 25) * canvas.clientWidth + "px";
    ctx.beginPath();
    ctx.lineWidth = "3px";
    for (let i = 0; i < data.length; i++) {
        let x = canvas.width * (i / (data.length - 1));
        let y = canvas.height * (data[i] / 4 + 1/2);
        ctx.lineTo(x, y);
    }
    ctx.stroke();
}


function RunCanvas() {
    $.ajax({
        url: "/signal",
        type: 'GET',
        dataType: 'json',
        success: (data) => {
            DrawData(data);
            setTimeout(RunCanvas, 256);
        }
    });
}


document.addEventListener('DOMContentLoaded', () => {
    setInterval(UpdatePulse, 1000);
    RunCanvas();
});
