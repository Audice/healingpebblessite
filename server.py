#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from http.server import HTTPServer, BaseHTTPRequestHandler
import numpy as np
import torch

global_signal = []
model = torch.jit.load("model.pt")
file = open("signal", 'wb')

v_to_del = {1:'p', 2:'qrs', 3:'t'}

def remove_small(signal):
    max_dist = 12
    last_zero = 0
    for i in range(len(signal)):
        if signal[i] == 0:
            if i - last_zero < max_dist:
                signal[last_zero:i] = 0
            last_zero = i

def merge_small(signal):
    max_dist = 12
    lasts = np.full(signal.max() + 1, -(max_dist+1))
    for i in range(len(signal)):
        m = signal[i]
        if i - lasts[m] < max_dist and m > 0:
            signal[lasts[m]:i] = m
        lasts[m] = i

def mask_to_delineation(mask):
    merge_small(mask)
    remove_small(mask)
    delineation = {'p':[], 'qrs':[], 't':[]}
    i = 0
    mask_length = len(mask)
    while i < mask_length:
        v = mask[i]
        if v > 0:
            delineation[v_to_del[v]].append([i, 0])
            while i < mask_length and mask[i] == v:
                delineation[v_to_del[v]][-1][1] = i
                i += 1
        i += 1
    return delineation


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello world (GET)")
    
    def do_POST(self):
        global global_signal, file
        self.send_response(200)
        self.end_headers()
        content_length = int(self.headers["Content-Length"])
        data = self.rfile.read(content_length)
        file.write(data)
        x = np.frombuffer(data, dtype=np.int16)
        x = (x - 2048) / 2000
        global_signal += x.tolist()
        if len(global_signal) >= 5000:
            qrs = self._get_qrs(global_signal[-5000:])
            self._write_pulse(qrs)
        
    def _get_qrs(self, signal):
        global model
        mask = model(torch.Tensor(signal).reshape(1, 1, -1))
        mask = mask[0].data.numpy().argmax(axis=0)
        delineation = mask_to_delineation(mask)
        return np.array(delineation["qrs"])[:, 0]

    def _write_pulse(self, qrs):
        pulse = "-"
        if len(qrs) > 1:
            qrs = np.array(qrs)
            qrs = np.diff(qrs)
            qrs = qrs / 500
            pulse = np.mean(60 / qrs)
            pulse = str(int(pulse))
        with open("pulse.txt", 'w') as f:
            f.write(pulse)

def run():
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, MyHandler)
    httpd.serve_forever()

run()
